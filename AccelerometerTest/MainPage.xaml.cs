﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Shapes;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using Microsoft.Devices.Sensors;

namespace AccelerometerTest
{
        public partial class MainPage : PhoneApplicationPage
    {
        Ball ball = new Ball();
        Accelerometer a = new Accelerometer();
        DispatcherTimer timer = new DispatcherTimer();

        public MainPage()
        {
            InitializeComponent();
            ball.Plane = new Canvas() { Width = Application.Current.Host.Content.ActualWidth, Height = Application.Current.Host.Content.ActualHeight };
            ball.SetColor((Color)this.Resources["PhoneAccentColor"]);
            ball.Radius = 100;
            ball.Pos = new Point((ball.Plane.Width - ball.Radius) / 2, (ball.Plane.Height - ball.Radius) / 2);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
            a.CurrentValueChanged += new EventHandler<SensorReadingEventArgs<AccelerometerReading>>(a_CurrentValueChanged);
            a.Start();
            LayoutRoot.Children.Add(ball.Plane);
        }

        void a_CurrentValueChanged(object sender, SensorReadingEventArgs<AccelerometerReading> e)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() => AccelerometerChanged(e.SensorReading));
        }

        void AccelerometerChanged(AccelerometerReading e)
        {
            ball.Accel = new Point(Math.Round(e.Acceleration.X, 2), -Math.Round(e.Acceleration.Y, 2));
        }

        void timer_Tick(object sender, EventArgs args)
        {
            ball.Update(timer.Interval.TotalSeconds);
            Canvas.SetLeft(ball.Back, ball.Pos.X);
            Canvas.SetTop(ball.Back, ball.Pos.Y);
            Canvas.SetLeft(ball.Blick, ball.Pos.X);
            Canvas.SetTop(ball.Blick, ball.Pos.Y);
        }
    }
}