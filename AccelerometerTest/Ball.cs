﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Shapes;
using System.Windows.Media;

namespace AccelerometerTest
{
    public class Ball
    {
        private Point pos, accel, speed;
        private Ellipse back, blick;
        private Canvas plane;
        private double g, k, bk;
        public Ball()
        {
            plane = new Canvas();
            back = new Ellipse();
            blick = new Ellipse()
            { Fill = new RadialGradientBrush(Color.FromArgb(128, 255, 255, 255), Color.FromArgb(0, 255, 255, 255))
            { GradientOrigin = new Point(0.6, 0.4), Center = new Point(0.6, 0.4), RadiusX = 0.5, RadiusY = 0.5 } };
            pos = new Point(0, 0);
            accel = new Point(0, 0);
            Radius = 100;
            g = 9.81;
            k = 9;
            bk = 0.65;
        }
        public Point Pos
        {
            get { return pos; }
            set { pos = value; }
        }
        public Point Accel
        {
            get { return accel; }
            set { accel = new Point(value.X * g, value.Y * g); }
        }
        public Point Speed
        {
            get { return speed; }
            set { speed = new Point(value.X * k, value.Y * k); }
        }
        public Ellipse Back
        {
            get { return back; }
            set { }
        }
        public Ellipse Blick
        {
            get { return blick; }
            set { }
        }
        public Canvas Plane
        {
            get { return plane; }
            set
            {
                plane = value;
                plane.Children.Add(this.back);
                plane.Children.Add(this.blick);
            }
        }
        public double Radius
        {
            get { return back.Height; }
            set
            {
                back.Height = value;
                back.Width = value;
                blick.Height = value;
                blick.Width = value;
            }
        }
        public void SetColor(Color value)
        {
            back.Fill = new SolidColorBrush(value);
        }
        public void Update(double time)
        {
            speed.X += accel.X * time * k;
            speed.Y += accel.Y * time * k;
            if (pos.X + speed.X < 0)
            {
                pos.X = 0;
                speed.X = -speed.X * bk;
            }
            else if (pos.X + speed.X > plane.Width - Radius)
            {
                pos.X = plane.Width - Radius;
                speed.X = -speed.X * bk;
            }
            else pos.X += speed.X;
            if (pos.Y + speed.Y < 0)
            {
                pos.Y = 0;
                speed.Y = -speed.Y * bk;
            }
            else if (pos.Y + speed.Y > plane.Height - Radius)
            {
                pos.Y = plane.Height - Radius;
                speed.Y = -speed.Y * bk;
            }
            else pos.Y += speed.Y;
        }
    }
}
